﻿using UnityEngine;
using System.Collections;

public class TextureApplier : MonoBehaviour
{
    public Material MaterialToApply;

    private ColorControl selectedObject;

    void OnEnable()
    {
        ColorControl.objectTapped += objectTappedHandler;
    }

    void OnDisable()
    {
        ColorControl.objectTapped -= objectTappedHandler;
    }

    private void objectTappedHandler(object sender, System.EventArgs e)
    {
        var args = e as objectTappedEventArgs;
        selectedObject = args.sender;
    }

    /// <summary>
    /// chamar pelos eventos da ui
    /// </summary>
    public void ApplyTexture()
    {
        selectedObject.ChangeMaterial(MaterialToApply);
    }
}
