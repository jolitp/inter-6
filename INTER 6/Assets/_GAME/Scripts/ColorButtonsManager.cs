﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ColorButtonsManager : MonoBehaviour
{
    [SerializeField] private GameObject panel;

    [SerializeField] private GameObject[] buttons;
    [SerializeField] private GameObject[] Images;

    public Sprite[] texturesFromobject;
    //void Awake()
    //{
    //    for (int i = 0; i < buttons.Length; i++)
    //    {
    //        texturesFromobject[i] = 
    //            buttons[i].GetComponentInChildren<Image>().mainTexture;
    //    }
    //}

    void OnEnable()
    {
        ColorControl.objectTapped += objectTappedHandler;
    }

    void OnDisable()
    {
        ColorControl.objectTapped -= objectTappedHandler;
    }

    private void objectTappedHandler(object sender, EventArgs e)
    {
        SetPanel(true);

        objectTappedEventArgs args = e as objectTappedEventArgs;
        ChangeButtonsTextures(args);
        SendTexturesToButtons(args);
    }

    public void TogglePanel()
    {
        panel.SetActive(!panel.activeInHierarchy);
    }

    public void SetPanel(bool state)
    {
        panel.SetActive(state);
    }

    private void ChangeButtonsTextures(objectTappedEventArgs args)
    {
        Sprite[] textures = new Sprite[3];
        for (int i = 0; i < textures.Length; i++)
        {
            if (i == textures.Length - 1)
            {
                textures[i] = args.rightTextureUI;
            }
            else
            {
                textures[i] = args.wrongTexturesUI[i];
            }
        }
        texturesFromobject = textures;
        ApplySpritesToButtons(textures);
    }

    private void ApplySpritesToButtons(Sprite[] textures)
    {
        for (int i = 0; i < textures.Length; i++)
        {
            var texture = Images[i].GetComponentInChildren<Image>();
            texture.sprite = texturesFromobject[i];
        }
    }

    private void SendTexturesToButtons(objectTappedEventArgs args)
    {
        Material[] materials = new Material[3];
        for (int i = 0; i < materials.Length; i++)
        {
            if (i == materials.Length - 1)
            {
                materials[i] = args.rightMaterial;
            }
            else
            {
                materials[i] = args.WrongMaterials[i];
            }
        }

        for (int i = 0; i < materials.Length; i++)
        {
            buttons[i].GetComponent<TextureApplier>().MaterialToApply = 
                materials[i];
        }
    }
}
