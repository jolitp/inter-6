﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using TouchScript.Hit;
using System;

public class ColorControl : MonoBehaviour
{
    [SerializeField] private Material startMaterial;
    private Material originalMaterial;

    [SerializeField] private Material rightMaterial;
    [SerializeField] private Material[] wrongMaterials;

    [SerializeField] private Sprite rightTextureUI;
    [SerializeField] private Sprite[] wrongTexturesUI;

    public static event EventHandler objectTapped;

    void Awake()
    {
        gameObject.AddComponent<TapGesture>();
        gameObject.AddComponent<MeshCollider>();

        var render = GetComponent<Renderer>();
        if (originalMaterial == null)
        {
            originalMaterial = render.material;
            render.material = startMaterial;
        }
    }

    void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
    }


    void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
    }


    private void tappedHandler(object sender, System.EventArgs e)
    {
        var args = new objectTappedEventArgs();
        args.WrongMaterials = wrongMaterials;
        args.rightMaterial = rightMaterial;

        args.rightTextureUI = rightTextureUI;
        args.wrongTexturesUI = wrongTexturesUI;

        args.sender = this;
        if (objectTapped != null) objectTapped(this, args);
        //ChangeMaterial();
    }

    public void ChangeMaterial(Material material)
    {
        var render = GetComponent<Renderer>();
        render.material = material;
    }
}
