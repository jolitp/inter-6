﻿using System;
using UnityEngine;

public class objectTappedEventArgs : EventArgs
{
    public Material rightMaterial;
    public Material[] WrongMaterials;

    public Sprite rightTextureUI;
    public Sprite[] wrongTexturesUI;

    public ColorControl sender;
}
